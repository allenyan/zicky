bremove()
{
    for pkg in $*;
    do
        brew rm $pkg
        while [ -n "`brew leaves | grep -vf ~/.brew_world`" ];
        do
            leftover=`brew leaves | grep -vf ~/.brew_world`
            echo $leftover | xargs brew rm --force
        done

        if [ -z `brew list | grep -Fx $pkg` ];
        then
            sed -ig '/'$pkg'/'d ~/.brew_world
        fi
    done
}

badd()
{
    for pkg in $*
    do
        brew install $pkg

        if [ ! -z `brew list | grep -Fx $pkg` ] && [ -z `grep -Fx $pkg ~/.brew_world` ];
        then
            echo $pkg >> ~/.brew_world
        fi
    done
}

export EDITOR="/usr/local/bin/vim"
alias baddbin="brew cask install"
alias bremovebin="brew cask uninstall"
alias bsearch="brew search"
alias bsearchbin="brew cask search"
alias bup="brew update && brew upgrade --all"
