#### Expand sbin path for zsh-completion ####
PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin:~/.local/bin

#### Neovim ####
#alias vim="nvim"

#### Aliases for Portage ####
alias eadd="sudo emerge --ask --verbose"
alias eremove="sudo emerge --ask --unmerge"
alias esearch="emerge --search"
alias eup="sudo emaint sync -a && sudo eix-update && sudo emerge -avuDN @world && sudo emerge -v @preserved-rebuild"
alias euw="sudo emerge -avuDN @world"
alias euwy="sudo emerge -vuDN @world"
alias ereb="sudo emerge @preserved-rebuild"

alias edmc="sudo vim /etc/portage/make.conf"
alias tailef="tail -f /var/log/emerge-fetch.log"
alias taile="tail -f /var/log/emerge.log"

#### Shortcut for probing vbox modules, seems unnecessary now ####
alias vboxrmmod='sudo rmmod vboxpci; sudo rmmod vboxnetflt; sudo rmmod vboxnetadp; sudo rmmod vboxdrv;'
alias vboxmodprobe='sudo modprobe vboxdrv; sudo modprobe vboxnetadp; sudo modprobe vboxnetflt; sudo modprobe vboxpci'
