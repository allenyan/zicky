seehost() {
    for hst in $*
    do
        grep $hst ~/adaptv/dns/*
    done
}

alias vpnconnect="sudo racoonctl vpn-connect 64.236.121.177"
alias vpndisconnect="sudo racoonctl vpn-disconnect 64.236.121.177"

alias ans="ssh ops@ansible.sj.adap.tv"
alias bbq="ssh ayan@bbq.hq.adap.tv"
