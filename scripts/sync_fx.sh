#!/bin/bash

#set -x

function disk_to_ram {
    rsync -a --delete --exclude 'lock' /home/$(logname)/.mozilla/firefox/${pname}.bak/ /dev/shm/${pname}
}

function ram_to_disk {
    rsync -a --delete --exclude 'lock' /dev/shm/${pname}/ /home/$(logname)/.mozilla/firefox/${pname}.bak
}

case $1 in
    'disk_to_ram')
        pname=$2
        disk_to_ram
        ;;
    'ram_to_disk')
        pname=$2
        ram_to_disk
        ;;
esac
