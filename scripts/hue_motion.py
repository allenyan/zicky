#!/usr/bin/env python

import time
import socket
import requests
import logging
import logging.handlers
import datetime as dt
from daemon import runner
from gpiozero import MotionSensor
from astral import Astral


class Light(object):
    def __init__(self, light_name):
        self.name = light_name
        self.is_on = False
        self.brightness = 0
        self.color_temp = 233
        self.full_bri = 254
        self.full_ct = 233
        self.dimmed_bri = 127
        self.dimmed_ct = 233


class HueMotion(object):

    def __init__(self, bridge_url, hue_username):
        self.stdin_path = "/dev/null"
        self.stdout_path = "/dev/tty"
        self.stderr_path = "/dev/tty"
        self.pidfile_path = "/var/run/hue_motion.pid"
        self.pidfile_timeout = 5
        self.no_move_threshold_sec = 1800    # in seconds
        self.scan_freq = 0.5    # in seconds
        self.no_move_threshold = self.no_move_threshold_sec / self.scan_freq
        self.no_move_count_down = self.no_move_threshold
        self.reset_hue_connection(bridge_url, hue_username)

    def reset_hue_connection(self, bridge_url, hue_username):
        self.hue_bridge_ip = socket.gethostbyname(bridge_url)
        self.user_key = hue_username
        self.hue_url = "http://" + self.hue_bridge_ip + "/api/" + self.user_key
        self.light_list = {
                "living": "3",
                "room": "5",
                "allen": "8"
                }

    def check_light(self, the_light):
        resp = requests.get(self.hue_url + "/lights/" + self.light_list[the_light.name])
        resp_json = resp.json()
        return resp_json["state"]

    def count_down(self, the_light):
        self.no_move_count_down -= 1
        logger.debug("counting down " + str(self.no_move_count_down))
        if self.no_move_count_down == self.no_move_threshold / 2:
            self.light_dimm(the_light)
        if self.no_move_count_down == 0:
            self.light_off(the_light)
            self.reset_count()

    def light_on(self, the_light):
        if the_light.name == "allen":
            cmd = {
                    "on": True,
                    "bri": the_light.full_bri,
                    "ct": the_light.full_ct
                  }
        else:
            cmd = {
                    "on": True,
                    "bri": the_light.full_bri
                  }
        if not the_light.is_on or (the_light.is_on and the_light.brightness != the_light.full_bri):
            logger.info("Turning on " + the_light.name)
            r = requests.put(self.hue_url + "/lights/" + self.light_list[the_light.name] + "/state", json=cmd)

    def light_dimm(self, the_light):
        if the_light.name == "allen":
            cmd = {
                    "on": True,
                    "bri": the_light.dimmed_bri,
                    "ct": the_light.dimmed_ct
                  }
        else:
            cmd = {
                    "on": True,
                    "bri": the_light.dimmed_bri
                  }
        if the_light.is_on and the_light.brightness == the_light.full_bri:
            logger.info("Dimming " + the_light.name)
            r = requests.put(self.hue_url + "/lights/" + self.light_list[the_light.name] + "/state", json=cmd)

    def light_off(self, the_light):
        cmd = {
                "on": False
              }
        if the_light.is_on:
            logger.info("Turning off " + the_light.name)
            r = requests.put(self.hue_url + "/lights/" + self.light_list[the_light.name] + "/state", json=cmd)

    def reset_count(self):
        logger.debug("resetting no_motion_count")
        self.no_move_count_down = self.no_move_threshold

    def run(self):
        logger.info("Starting hue_motion daemon")

        pir = MotionSensor(4)
        the_light = Light(light_name)

        time_delta = dt.timedelta(minutes=30)
        a = Astral()
        a.solar_depression = "civil"
        city_name = "San Francisco"
        city = a[city_name]
        sun = city.sun(date=dt.date.today(), local=True)
        stime = sun["sunset"].time()
        start_time = (dt.datetime.combine(dt.date(1, 1, 1), stime) - time_delta).time()
        logger.info("Updating start time to " + str(start_time))

        self.reset_count()

        while True:
            if dt.datetime.now().time() <= dt.datetime(1, 1, 1, 0, 0, 0, 500000).time():    #update sunset/start time in the first second everyday
                sun = city.sun(date=dt.date.today(), local=True)
                stime = sun["sunset"].time()
                start_time = (dt.datetime.combine(dt.date(1, 1, 1), stime) - time_delta).time()
                logger.info("Updating start time to " + str(start_time))
            if dt.datetime.now().time() >= start_time or the_light.is_on:
                try:
                    light_state = self.check_light(the_light)
                except requests.exceptions.ConnectionError as e:
                    logger.error("Error: %s" % e)
                    time.sleep(5)
                    logger.info("Resetting Hue bridge connection...")
                    self.reset_hue_connection(bridge_url, hue_username)
                    continue
                the_light.brightness = light_state["bri"]
                the_light.is_on = light_state["on"]
                if pir.motion_detected:
                    self.reset_count()
                    self.light_on(the_light)
                elif the_light.is_on:
                    self.count_down(the_light)
            time.sleep(self.scan_freq)


def init_logger():
    formatter = logging.Formatter("%(asctime)s - %(message)s")
    handler = logging.handlers.RotatingFileHandler("/var/log/hue_motion/hue_motion.log", maxBytes=1000000, backupCount=5)
    handler.setLevel(logging.INFO)
    handler.setFormatter(formatter)
    logging.getLogger().addHandler(handler)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    return handler, logger


if __name__ == "__main__":
    bridge_url = "allenph.jjay"
    hue_username = "UbJEFDtnagHw4CvItCRW8kQIstErIdeOu7ST4IQ3"
    light_name = "allen"

    handler, logger = init_logger()

    hue_motion = HueMotion(bridge_url, hue_username)

    daemon_runner = runner.DaemonRunner(hue_motion)
    daemon_runner.daemon_context.files_preserve = [handler.stream]
    daemon_runner.do_action()
