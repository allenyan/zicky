#!/bin/bash

function install_zicky() {
    # clone zicky repo
    printf "${BLUE}Cloning zicky repository...${NORMAL}\n"
    cd ~
    mkdir jigra
    cd jigra
    git clone https://allenyan@bitbucket.org/allenyan/zicky.git

    # create symlinks to custom plugins and themes
    printf "${BLUE}Creating symlinks in omz to custom plugins and themes...${NORMAL}\n"
    cd ~/.oh-my-zsh/custom
    ln -s ~/jigra/zicky/zsh/themes
    cd plugins
    ln -s ~/jigra/zicky/zsh/plugins/* .

    printf "${BLUE}Cloning zsh-autosuggestions and zsh-syntax-highlighting...${NORMAL}\n"
    cd ~/jigra/zicky/zsh/plugins
    git clone https://github.com/zsh-users/zsh-autosuggestions.git
    git clone git://github.com/zsh-users/zsh-syntax-highlighting.git

    printf "${BLUE}Final tasks...${NORMAL}\n"
    cd ~
    rm .zshrc
    ln -s ~/jigra/zicky/zsh/zshrc_${distro} .zshrc
    ln -s ~/jigra/zicky/zsh/zshrc_common .zshrc_common

    printf "${GREEN}Done!${NORMAL}\n"
    exit 1
}

function install_omz() {
    # Only enable exit-on-error after the non-critical colorization stuff,
    # which may fail on systems lacking tput or terminfo
    set -e

    CHECK_ZSH_INSTALLED=$(grep /zsh$ /etc/shells | wc -l)
    if [ ! $CHECK_ZSH_INSTALLED -ge 1 ]; then
        printf "${YELLOW}Zsh is not installed!${NORMAL} Please install zsh first!\n"
        exit
    fi
    unset CHECK_ZSH_INSTALLED

    if [ ! -n "$ZSH" ]; then
        ZSH=~/.oh-my-zsh
    fi

    if [ -d "$ZSH" ]; then
        printf "${YELLOW}You already have Oh My Zsh installed.${NORMAL}\n"
        printf "You'll need to remove $ZSH if you want to re-install.\n"
        exit
    fi

    # Prevent the cloned repository from having insecure permissions. Failing to do
    # so causes compinit() calls to fail with "command not found: compdef" errors
    # for users with insecure umasks (e.g., "002", allowing group writability). Note
    # that this will be ignored under Cygwin by default, as Windows ACLs take
    # precedence over umasks except for filesystems mounted with option "noacl".
    umask g-w,o-w

    printf "${BLUE}Cloning Oh My Zsh...${NORMAL}\n"
    hash git >/dev/null 2>&1 || {
        echo "Error: git is not installed"
        exit 1
    }
    env git clone --depth=1 https://github.com/robbyrussell/oh-my-zsh.git $ZSH || {
        printf "Error: git clone of oh-my-zsh repo failed\n"
        exit 1
    }

    # The Windows (MSYS) Git is not compatible with normal use on cygwin
    if [ "$OSTYPE" = cygwin ]; then
        if git --version | grep msysgit > /dev/null; then
            echo "Error: Windows/MSYS Git is not supported on Cygwin"
            echo "Error: Make sure the Cygwin git package is installed and is first on the path"
            exit 1
        fi
    fi

    printf "${BLUE}Looking for an existing zsh config...${NORMAL}\n"
    if [ -f ~/.zshrc ] || [ -h ~/.zshrc ]; then
        printf "${YELLOW}Found ~/.zshrc.${NORMAL} ${GREEN}Backing up to ~/.zshrc.pre-oh-my-zsh${NORMAL}\n";
        mv ~/.zshrc ~/.zshrc.pre-oh-my-zsh;
    fi

    printf "${BLUE}Using the Oh My Zsh template file and adding it to ~/.zshrc${NORMAL}\n"
    cp $ZSH/templates/zshrc.zsh-template ~/.zshrc
    sed "/^export ZSH=/ c\\
    export ZSH=$ZSH
    " ~/.zshrc > ~/.zshrc-omztemp
    mv -f ~/.zshrc-omztemp ~/.zshrc

    printf "${BLUE}Copying your current PATH and adding it to the end of ~/.zshrc for you.${NORMAL}\n"
    sed "/export PATH=/ c\\
    export PATH=\"$PATH\"
    " ~/.zshrc > ~/.zshrc-omztemp
    mv -f ~/.zshrc-omztemp ~/.zshrc

    # If this user's login shell is not already "zsh", attempt to switch.
    TEST_CURRENT_SHELL=$(expr "$SHELL" : '.*/\(.*\)')
    if [ "$TEST_CURRENT_SHELL" != "zsh" ]; then
        # If this platform provides a "chsh" command (not Cygwin), do it, man!
        if hash chsh >/dev/null 2>&1 && grep $(logname) /etc/passwd >/dev/null 2>&1; then
            printf "${BLUE}Time to change your default shell to zsh!${NORMAL}\n"
            chsh -s $(grep /zsh$ /etc/shells | tail -1)
        # Else, suggest the user do so manually.
        else
            printf "I can't change your shell automatically because this system does not have chsh or you are using LDAP.\n"
            #printf "${BLUE}Please manually change your default shell to zsh!${NORMAL}\n"
            printf "Adding zsh to ~/.profile\n"
            echo "" >> ~/.profile
            echo "export SHELL='/bin/zsh" >> ~/.profile
            echo "exec /bin/zsh" >> ~/.profile
        fi
    fi
}


# Use colors, but only if connected to a terminal, and that terminal
# supports them.
if which tput >/dev/null 2>&1; then
    ncolors=$(tput colors)
fi
if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
    RED="$(tput setaf 1)"
    GREEN="$(tput setaf 2)"
    YELLOW="$(tput setaf 3)"
    BLUE="$(tput setaf 4)"
    BOLD="$(tput bold)"
    NORMAL="$(tput sgr0)"
else
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    NORMAL=""
fi


if [ -z $1 ]; then
    echo "pls enter distro"
    exit 1
else
    distro=$1
fi

dlist="zsh git autojump"
mlist=""
missd=0

for prog in $dlist; do
    if ! hash $prog >/dev/null 2>&1; then
        echo "$prog is not installed"
        missd=1
        mlist+=" $prog"
    fi
done

if [ $missd -eq 1 ]; then
    if [ $distro == "ubuntu" ]; then
        sudo apt-get -y install $mlist
    elif [ $distro == "osx" ]; then
        brew install $mlist
    elif [ $distro == "gentoo" ]; then
        sudo emerge -v $mlist
    else
        echo "disto is unsupported"
        exit 1
    fi
fi

install_omz
install_zicky
